#
# Makefile
# 
# Created 7 Dec 2022 by Mason Hall (mason@mashpot.net)
#
# Copyright (C)  Mason Hall
#

OUTPUT = ccsync
CC = gcc
CFLAGS = -fdiagnostics-color=always -O3 -g -Wall
INCLUDES = 
LIBS = -lc 
FILES = main.c

SRCS = $(addprefix ./src/, $(FILES))
OBJS = $(addprefix ./build/src/, $(FILES:.c=.o))
YEAR := $(shell date +%Y)

default: all

all: $(OUTPUT)

$(OUTPUT) : $(OBJS)
	@echo '$@'
	$(CC) $(CFLAGS) $(OBJS) $(LIBS) -o $@ 

build/src/%.o : ./src/%.c
	mkdir -p $(@D)
	@echo 'Building $<'
	$(CC) $(CFLAGS) $(INCLUDES) -D__YEAR__=$(YEAR) -MD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c "$<" -o "$@" 
	@echo ' '

clean:
	-rm -f $(OUTPUT)
	-rm -f $(OBJS)

