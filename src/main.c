/*
 * main.c
 *
 * Created 14 Feb 2023 by Mason Hall (mason@mashpot.net)
 *
 * Copyright (C) 2023 Mason Hall
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>

#include <sys/types.h>
#include <dirent.h>

#include <sys/inotify.h>

#define INOT_FLAGS 0
#define INOT_MASK ( \
    IN_DELETE | \
    IN_DELETE_SELF | \
    IN_MODIFY | \
    IN_MOVE   \
)

#define IN_EV_SIZE sizeof(struct inotify_event)
#define IN_EV_MAX (IN_EV_SIZE + 4096 + 1)
#define BUFMAX (IN_EV_MAX * 128)

#define ESC_FMT "ccsync: \e[1;92;49m"

struct rename_event {
    struct rename_event *next;
    const struct inotify_event *event;
};

struct watchent {
    int wd;
    unsigned len;
    char *name;
};

static struct watchent *watchv = NULL;
static size_t watchc = 0;

static int running = 1;

static void int_handler(int sig, siginfo_t *info, void *context)
{
    running = 0;
    putchar('\r');
}

struct watchent *find_watchent(int wd)
{
    struct watchent *ent = NULL;
    size_t i;
    for (i = 0; i < watchc; ++i) {
        if (watchv[i].wd == wd) {
            ent = &watchv[i];
            break;
        }
    }
    return ent;
}

const char *event_name(const struct inotify_event *event)
{
    static char name[8192];
    struct watchent *ent;
    size_t len, n;

    ent = find_watchent(event->wd);
    if (!ent)
        return NULL;
    len = ent->len;
    memcpy(&name[0], ent->name, len);
    if (name[len-1] != '/') {
        name[len] = '/';
        len++;
    }
    if (event->len) {
        n = strlen(&event->name[0]);
        memcpy(&name[len], &event->name[0], n);
        len += n;
    }
    name[len] = 0;
    return &name[0];
}

int check_name(const char *name)
{
    size_t len;
    const char *p;

    len = strlen(name);
    p = name + len - 1;

    if (*p == '~')
        return 0;
    while (p > name) {
        if (*p == '.')
            return 1;
        if (*p == 0 || *p == '/')
            break;
        p--;
    }
    return 0;
}

int dir_add_watch(char *name, unsigned len, int fd, int events)
{
    struct watchent *ent;
    int wd = inotify_add_watch(fd, name, events);
    if (wd == -1) {
        fprintf(stderr, "ccsync: %s \"%s\"\n", strerror(errno), name);
        return -1;
    }
    if (!watchv) {
        watchv = malloc(sizeof(struct watchent));
        if (!watchv) {
            perror("ccsync");
            return -1;
        }
    } else {
        watchv = realloc(watchv, sizeof(struct watchent) * (watchc+1));
        if (!watchv) {
            perror("ccsync");
            return -1;
        }
    }
    ent = &watchv[watchc];
    ent->wd = wd;
    ent->len = len;
    ent->name = name;
    watchc++;
    printf("add: %s\n", name);
    return 0;
}

int dir_recurse(const char *pathname, int fd, int events)
{
    struct dirent *ent;
    int errsv, rv = 0, doslash = 0;
    DIR *dir;
    char *path, *p;
    char **pathv = NULL;
    size_t pathc = 0;
    size_t len;

    dir = opendir(pathname);
    if (!dir) {
        perror("opendir");
        return -1;
    }

    for (errsv = errno; ; errsv = errno) {
        ent = readdir(dir);
        if (!ent) {
            if (errsv != errno) {
                perror("readdir");
                rv = -1;
                goto error;
            }
            break;
        }
        if (ent->d_name[0] == '.')
            continue;

        if (ent->d_type == DT_DIR) {
            len = strlen(pathname);
            if (pathname[len-1] != '/') {
                doslash = 1;
                len++;
            }
            len += strlen(&ent->d_name[0]);

            path = malloc(len+1);
            if (!path) {
                perror("ccsync");
                rv = -1;
                goto error;
            }
            p = stpcpy(path, pathname);
            if (doslash)
                *p++ = '/';
            strcpy(p, &ent->d_name[0]);

            if (dir_add_watch(path, len, fd, events) == -1) {
                free(path);
                rv = -1;
                goto error;
            }

            if (!pathv) {
                pathv = malloc(sizeof(char*));
            } else {
                pathv = realloc(pathv, sizeof(char*) * (pathc+1));
            }
            if (!pathv) {
                perror("ccsync");
                rv = -1;
                goto error;
            }
            pathv[pathc] = path;
            pathc++;
        }
    }


error:
    closedir(dir);

    for (len = 0; len < pathc; ++len) {
        path = pathv[len];
        dir_recurse(path, fd, events);
        //if (rv != 0)
        //    free(path);
    }
    if (pathv)
        free(pathv);
    return rv;
}

int watch_events(int fd)
{
    char buf[BUFMAX];
    void *p;
    ssize_t n;
    int docompile = 0;

    struct rename_event *renames = NULL;
    struct rename_event **pprev, *rn;

    const struct inotify_event *event;

    for (; running;) {
        n = read(fd, &buf[0], sizeof(buf));
        if (n == -1 && errno != EINTR) {
            fprintf(stderr, "ccsync: %s\n", strerror(errno));
            exit(1);
        }
        if (n <= 0)
            continue;
        if (docompile) {
            docompile--;
            continue;
        }

        for (p = &buf[0]; p < (void*) &buf[n];
             p += IN_EV_SIZE + event->len) {

            event = (const struct inotify_event*) p;

            if (event->mask & IN_DELETE_SELF) {
                printf("ccsync: Directory was deleted \"%s\"\n", event_name(event));
                exit(1);
            }
            if (!event->len || (event->mask & IN_ISDIR) ||
                    event->name[0] == '.' || !check_name(&event->name[0]))
                continue;

            if (event->mask & IN_MOVED_FROM) {
                rn = malloc(sizeof(struct rename_event));
                if (!rn) {
                    perror("ccsync");
                    exit(-1);
                }
                rn->next = renames;
                rn->event = event;
                renames = rn;
                continue;
            } else if (event->mask & IN_MOVED_TO) {
                for (pprev = &renames, rn = renames;
                     rn != NULL;
                     pprev = &rn->next, rn = rn->next) {

                    if (rn->event->cookie == event->cookie) {
                        *pprev = rn->next;
                        printf(ESC_FMT"[event]\e[0m move: \"%s\" -> \"%s\"\n",
                                event_name(rn->event), event_name(event));
                        free(rn);
                        break;
                    }
                }
            } else if (event->mask & IN_DELETE) {
                printf(ESC_FMT"delete:\e0m \"%s\"\n", event_name(event));
            } else if (event->mask & IN_MODIFY) {
                printf(ESC_FMT"modify:\e[0m \"%s\"\n", event_name(event));
            }

            docompile = 1;
        }

        if (docompile) {
            printf("--------------------------------\n");
            if (system("make all") != 0) {
                fprintf(stderr, "ccsync: Compile error\n");
            }
            printf("--------------------------------\n");
        }
    }

    close(fd);
    return 0;
}

/*
 * ccSync
 */

const char source_dir[] = "/src";

int main(int argc, char *argv[])
{
    struct sigaction act;
    char *name, *p;
    size_t len;
    int fd;

    name = get_current_dir_name();
    if (!name) {
        fprintf(stderr, "ccsync: Cannot get current directory %s\n", strerror(errno));
        exit(1);
    }
    len = strlen(name);
    p = malloc(len + sizeof(source_dir));
    memcpy(p, name, len);
    memcpy(p+len, &source_dir[0], sizeof(source_dir));
    free(name);
    name = p;
    len += sizeof(source_dir)-1;

    memset(&act, 0, sizeof(act));
    act.sa_sigaction = int_handler;
    act.sa_flags = SA_SIGINFO;
    sigemptyset(&act.sa_mask);

    if (sigaction(SIGINT, &act, NULL) == -1) {
        perror("sigaction");
        return 1;
    }


    fd = inotify_init1(INOT_FLAGS);
    if (fd == -1) {
        perror("ccsync");
        exit(1);
    }

    if (dir_add_watch(name, len, fd, INOT_MASK) == -1)
        return 1;
    if (dir_recurse(name, fd, INOT_MASK) == -1)
        return 1;
    printf("ccsync: %s\n", name);

    return watch_events(fd);
}

